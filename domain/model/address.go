package model

type Address struct {
	Id      string `json:"id"`
	UserId  string `json:"user_id"`
	Address string `json:"address"`
	City    string `json:"city"`
	Country string `json:"country"`
	Zip     string `json:"zip"`
}
