package model

import "time"

type Orders struct {
	ID     string    `json:"id"`
	UserId string    `json:"user_id"`
	Date   time.Time `json:"date"`
	Statue string    `json:"statue"`
}
