package model

import (
	"encoding/json"
)

type Product struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Price       int    `json:"price"`
	Stock       int    `json:"stock"`
	Categorie   string `json:"categorie"`
	Image       []byte `json:"image"`
}

type Products []Product

func (p *Product) MarshalBinary() ([]byte, error) {
	return json.Marshal(p)
}

func (p *Product) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &p); err != nil {
		return err
	}
	return nil
}
