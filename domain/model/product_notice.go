package model

import "time"

type ProductNotice struct {
	Id        string    `json:"id"`
	ProductId string    `json:"product_id"`
	UserId    string    `json:"user_id"`
	Note      int       `json:"note"`
	Notice    string    `json:"notice"`
	Date      time.Time `json:"date"`
}
