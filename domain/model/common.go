package model

type ResponsBody struct {
	Success bool
	Status  int
	Data    []interface{}
}
