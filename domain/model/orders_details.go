package model

type OrdersDetails struct {
	Id        string `json:"id"`
	OrderId   string `json:"order_id"`
	ProductId string `json:"product_id"`
	Quantity  int    `json:"quantity"`
	UnitPrice int    `json:"unit_price"`
}
