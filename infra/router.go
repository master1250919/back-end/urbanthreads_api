package infra

import (
	"context"
	"github.com/redis/go-redis/v9"
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/logger"
	route "gitlab.com/Titouan-Esc/api_common/router"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/interfaces/controller"
)

func Dispatch(sql handler.SQLHandler, logServ, logSess *logger.Log, ctx context.Context) *route.Router {
	router := route.NewRouter()
	rdb, err := ConnectRedis(ctx)
	if err != nil {
		logServ.Error("[REDIS] ", err.Error())
	}

	userController := controller.NewUserController(sql, logSess)
	productController := controller.NewProductController(sql, logSess, rdb, ctx)

	router.AddCORS()

	//-------------------------- USER -----------------------------
	router.AddRoute(" Retrieve all users ", "POST", "/user/getAll", userController.ShowAll, logServ)
	router.AddRoute(" Retrieve one user ", "POST", "/user/getOne", userController.ShowOne, logServ)
	router.AddRoute(" Create user ", "POST", "/user/create", userController.Insert, logServ)
	router.AddRoute(" Update user ", "POST", "/user/update", userController.Modify, logServ)
	router.AddRoute(" Delete user ", "POST", "/user/delete", userController.Destroy, logServ)
	router.AddRoute(" Login ", "POST", "/user/login", userController.Connect, logServ)
	router.AddRoute(" Password Reset ", "POST", "/user/passwordReset", userController.PasswordReset, logServ)
	router.AddRoute(" Send SMS ", "POST", "/user/sendSms", userController.SendSMS, logServ)

	//-------------------------- PRODUCT --------------------------
	router.AddRoute(" Retrieve all products ", "POST", "/product/getAll", productController.ShowAll, logServ)
	router.AddRoute(" Retrieve one product ", "POST", "/product/getOne", productController.ShowOne, logServ)
	router.AddRoute(" Create product ", "POST", "/product/create", productController.Insert, logServ)
	router.AddRoute(" Update product ", "POST", "/product/update", productController.Modify, logServ)
	router.AddRoute(" Delete product ", "POST", "/product/delete", productController.Destroy, logServ)
	router.AddRoute(" Retrevie all products from PostegreSQL and insert in Redis ", "POST", "/product/insertRedis", productController.ShowAllAndInsertInDBRedis, logServ)
	router.AddRoute(" Retrieve all products from Redis DB ", "POST", "/product/getAllRedis", productController.ShowAllInRedis, logServ)

	//-------------------------- PING ----------------------------
	router.AddRoute(" Ping ", "POST", "/ping", userController.PING, logServ)

	return router
}

func ConnectRedis(ctx context.Context) (*redis.Client, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	_, err := rdb.Ping(ctx).Result()
	if err != nil {
		return nil, err
	}

	return rdb, nil
}
