package main

import (
	"context"
	"fmt"
	httpSwagger "github.com/swaggo/http-swagger"
	env2 "gitlab.com/Titouan-Esc/api_common/env"
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/logger"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/docs"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/infra"
	"net/http"
	"os"
)

// @title Urban Threads's swagger
// @version 1.0
// @description The swagger to urban threads'api.
// @termsOfService http://swagger.io/terms/
func main() {
	ctx := context.Background()
	env := env2.NewEnv()
	env.LoadEnv()

	logServ := logger.GetLoggers().Server
	logSess := logger.GetLoggers().Session

	sql, err := handler.NewSqlHandler()
	if err != nil {
		logServ.Error("[DB] ", err.Error())
		return
	}

	r := infra.Dispatch(sql, logServ, logSess, ctx)

	addr := os.Getenv("API_SERVER_HOST") + ":" + os.Getenv("API_SERVER_PORT")
	addrSwag := os.Getenv("API_HOST_SWAG") + ":" + os.Getenv("API_SERVER_PORT")

	docs.SwaggerInfo.Host = addrSwag

	r.Handle.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("https://"+addrSwag+"/swagger/doc.json"),
	))

	fmt.Println("Le serveur à bien était lancé")

	if err := http.ListenAndServeTLS(addr, os.Getenv("API_CERT_FULLCHAIN"), os.Getenv("API_CERT_PRIVKEY"), r.Handle); err != nil {
		logServ.Error("[RUN] ", err.Error())
		return
	}
}
