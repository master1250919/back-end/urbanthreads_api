package interactor

import (
	"errors"
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/utils"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"
	repository2 "gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/interfaces/repository"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/usecase/repository"
	"gorm.io/gorm"
)

type UserInteractor struct {
	UserRepository repository.IUserRepository
}

func NewUserIntercator(database *gorm.DB) *UserInteractor {
	return &UserInteractor{
		UserRepository: repository2.UserRepository{
			Sql: handler.SQLHandler{
				Db: database,
			},
		},
	}
}

func (ui *UserInteractor) ShowAll() (users model.Users, err error) {
	users, err = ui.UserRepository.GetAll()
	return
}

func (ui *UserInteractor) ShowOne(email string) (user model.User, err error) {
	user, err = ui.UserRepository.GetOne(email)
	return
}

func (ui *UserInteractor) ShowOneById(uid string) (user model.User, err error) {
	user, err = ui.UserRepository.GetOneId(uid)
	return
}

func (ui *UserInteractor) Insert(data model.User) (model.User, error) {

	exist := ui.UserRepository.IsExist(data.Email)

	if exist {
		return model.User{}, errors.New("User Already Exist for the e-mail")
	}

	user, err := ui.UserRepository.Create(data)
	if err != nil {
		return user, err
	}

	return user, nil
}

func (ui *UserInteractor) Modify(target, patch model.User) (err error) {

	exist := ui.UserRepository.IsExist(target.Email)

	if !exist {
		return errors.New("User don't exist, we can update them")
	}

	if err := utils.Bind(&target, patch); err != nil {
		return err
	}

	err = ui.UserRepository.Update(target)
	return
}

func (ui *UserInteractor) Destroy(uid string) (err error) {

	user, err := ui.UserRepository.GetOneId(uid)
	if err != nil {
		return err
	}

	exist := ui.UserRepository.IsExist(user.Email)

	if !exist {
		return errors.New("User don't exist, we can delete them")
	}

	err = ui.UserRepository.Delete(uid)
	return
}

func (ui *UserInteractor) Connect(email, password string) (err error) {
	err = ui.UserRepository.Login(email, password)
	return
}

func (ui *UserInteractor) PasswordReset(email string) error {

	exist := ui.UserRepository.IsExist(email)

	if !exist {
		return errors.New("User not exist in the database")
	}

	return nil
}
