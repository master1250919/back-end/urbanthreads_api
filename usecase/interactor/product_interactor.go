package interactor

import (
	"errors"
	"github.com/google/uuid"
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/utils"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"
	repository2 "gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/interfaces/repository"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/usecase/repository"
	"gorm.io/gorm"
)

type ProductInteractor struct {
	ProductRepository repository.IProductRepository
}

func NewProductInteractor(database *gorm.DB) *ProductInteractor {
	return &ProductInteractor{
		ProductRepository: repository2.ProductRepository{
			Sql: handler.SQLHandler{
				Db: database,
			},
		},
	}
}

func (pi *ProductInteractor) ShowAll() (model.Products, error) {
	products, err := pi.ProductRepository.GetAll()
	if err != nil {
		return nil, err
	}

	return products, nil
}

func (pi *ProductInteractor) ShowOne(name string) (model.Product, error) {
	product, err := pi.ProductRepository.GetOne(name)
	if err != nil {
		return model.Product{}, err
	}

	return product, nil
}

func (pi *ProductInteractor) ShowOneBydId(uid string) (model.Product, error) {
	product, err := pi.ProductRepository.GetOneById(uid)
	if err != nil {
		return model.Product{}, err
	}

	return product, nil
}

func (pi *ProductInteractor) Insert(data model.Product) error {

	data.Id = uuid.New().String()

	exist := pi.ProductRepository.IsExist(data.Name, "name")

	if exist {
		return errors.New("Product already exist")
	}

	if err := pi.ProductRepository.Create(data); err != nil {
		return err
	}

	return nil
}

func (pi *ProductInteractor) Modify(target, patch model.Product) error {

	exist := pi.ProductRepository.IsExist(target.Name, "name")

	if !exist {
		return errors.New("Product don't exist for update")
	}

	if err := utils.Bind(&target, patch); err != nil {
		return err
	}

	if err := pi.ProductRepository.Update(target); err != nil {
		return err
	}

	return nil
}

func (pi *ProductInteractor) Destroy(uid string) error {

	exist := pi.ProductRepository.IsExist(uid, "id")

	if !exist {
		return errors.New("Product don't exist for delete")
	}

	if err := pi.ProductRepository.Delete(uid); err != nil {
		return err
	}

	return nil
}

func (pi *ProductInteractor) ShowAllAndInsertInDBRedis(products model.Products) error {
	if err := pi.ProductRepository.InsertInRedis(products); err != nil {
		return err
	}

	return nil
}

func (pi *ProductInteractor) ShowAllInRedis() (model.Products, error) {
	products, err := pi.ProductRepository.GetAllInRedis()
	if err != nil {
		return nil, err
	}

	return products, nil
}
