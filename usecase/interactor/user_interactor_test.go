package interactor

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/Titouan-Esc/api_common/test"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"
	"gorm.io/gorm"
	"os"
	"testing"
)

type UserInteractorSetup struct {
	UI      *UserInteractor
	DB      *gorm.DB
	Suite   *test.Suite[model.User]
	NewUser model.User
	UID     string
}

func NewUserInteractorSetup(t *testing.T) *UserInteractorSetup {
	suite, db, redis, err := test.SetUp[model.User]("user_interactor_test.db")
	assert.Nil(t, err)
	assert.Nil(t, redis)

	ui := NewUserIntercator(db)

	errMigrate := db.AutoMigrate(model.User{})
	assert.Nil(t, errMigrate)

	return &UserInteractorSetup{
		UI:    ui,
		DB:    db,
		Suite: suite,
	}
}

func (s *UserInteractorSetup) TestUserInteractor_Insert(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Insert",
		Body: model.User{
			Name:     "Test",
			Email:    "test@test.com",
			Password: "Test",
			Phone:    "00.00.00.00.00",
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		user, err := s.UI.Insert(s.Suite.Data.Body)
		assert.Nil(t, err)

		s.UID = user.Id

		retour := s.DB.Table("users").Where("name = ?", s.Suite.Data.Body.Name).First(&s.NewUser)
		assert.Nil(t, retour.Error)

		assert.Equal(t, s.NewUser.Id, user.Id)
		assert.Equal(t, s.NewUser.Name, user.Name)
	})
}

func (s *UserInteractorSetup) TestUserInteractor_Connect(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Connect",
		Body: model.User{
			Email:    "test@test.com",
			Password: "Test",
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		err := s.UI.Connect(s.Suite.Data.Body.Email, s.Suite.Data.Body.Password)
		assert.Nil(t, err)
	})
}

func (s *UserInteractorSetup) TestUserInteractor_ShowAll(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Show All",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		users, err := s.UI.ShowAll()
		assert.Nil(t, err)

		assert.NotEmpty(t, users)
	})
}

func (s *UserInteractorSetup) TestUserInteractor_ShowOne(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Show One",
		Body: model.User{
			Email: "test@test.com",
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		user, err := s.UI.ShowOne(s.Suite.Data.Body.Email)
		assert.Nil(t, err)

		assert.NotEmpty(t, user)
		assert.Equal(t, s.Suite.Data.Body.Email, user.Email)
	})
}

func (s *UserInteractorSetup) TestUserInteractor_ShowOneById(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Show One By ID",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		user, err := s.UI.ShowOneById(s.UID)
		assert.Nil(t, err)

		assert.NotEmpty(t, user)
		assert.Equal(t, s.UID, user.Id)
	})
}

func (s *UserInteractorSetup) TestUserInteractor_Modify(t *testing.T) {
	var user model.User
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Modify",
		Body: model.User{
			Name:  "NewTest",
			Email: "newTest@newTest.com",
			Phone: "06.72.13.51.72",
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		retour := s.DB.Table("users").Where("id = ?", s.UID).First(&user)
		assert.Nil(t, retour.Error)

		err := s.UI.Modify(user, s.Suite.Data.Body)
		assert.Nil(t, err)

		result := s.DB.Table("users").Where("id = ?", user.Id).First(&s.NewUser)
		assert.Nil(t, result.Error)

		// IF is NOT Empty
		assert.NotEmpty(t, s.NewUser)

		// IF is Equal
		assert.Equal(t, user.Id, s.NewUser.Id)
		assert.Equal(t, user.Password, s.NewUser.Password)

		// IF is NOT Equal
		assert.NotEqual(t, user.Name, s.NewUser.Name)
		assert.NotEqual(t, user.Email, s.NewUser.Email)
		assert.NotEqual(t, user.Phone, s.NewUser.Phone)
	})
}

func (s *UserInteractorSetup) TestUserInteractor_Destroy(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Destroy",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		err := s.UI.Destroy(s.UID)
		assert.Nil(t, err)

		retour := s.DB.Table("users").Where("id = ?", s.UID).First(&s.NewUser)
		assert.NotNil(t, retour.Error)
	})
}

func TestUserIntercator(t *testing.T) {
	// Initialisation de l'environnement de test
	s := NewUserInteractorSetup(t)

	// TEST
	s.TestUserInteractor_Insert(t)
	s.TestUserInteractor_Connect(t)
	s.TestUserInteractor_ShowAll(t)
	s.TestUserInteractor_ShowOne(t)
	s.TestUserInteractor_ShowOneById(t)
	s.TestUserInteractor_Modify(t)
	s.TestUserInteractor_Destroy(t)

	// Suppression du fichier .db
	defer func() {
		if err := os.Remove("user_interactor_test.db"); err != nil {
			t.Log(err.Error())
		}
	}()
}
