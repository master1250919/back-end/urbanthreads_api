package interactor

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/Titouan-Esc/api_common/test"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"
	"gorm.io/gorm"
	"os"
	"testing"
)

type ProductInteractorSetup struct {
	PI         *ProductInteractor
	DB         *gorm.DB
	Suite      *test.Suite[model.Product]
	NewProduct model.Product
	UID        string
}

func NewProductInteractorSetup(t *testing.T) *ProductInteractorSetup {
	suite, db, redis, err := test.SetUp[model.Product]("product_interactor_test.db")
	assert.Nil(t, err)
	assert.Nil(t, redis)

	pi := NewProductInteractor(db)

	errMigrate := db.AutoMigrate(model.Product{})
	assert.Nil(t, errMigrate)

	return &ProductInteractorSetup{
		PI:         pi,
		DB:         db,
		Suite:      suite,
		NewProduct: model.Product{},
	}
}

func (s *ProductInteractorSetup) TestProductInteractor_Insert(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Interactor Insert",
		Body: model.Product{
			Name:        "Test",
			Description: "Test",
			Price:       100,
			Stock:       100,
			Categorie:   "Test",
			Image:       []byte("Test"),
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		err := s.PI.Insert(s.Suite.Data.Body)
		assert.Nil(t, err)

		retour := s.DB.Table("products").Where("name = ?", s.Suite.Data.Body.Name).First(&s.NewProduct)
		assert.Nil(t, retour.Error)

		s.UID = s.NewProduct.Id

		assert.NotEmpty(t, s.NewProduct)
		assert.Equal(t, s.Suite.Data.Body.Name, s.NewProduct.Name)
		assert.Equal(t, s.Suite.Data.Body.Stock, s.NewProduct.Stock)
	})

}

func (s *ProductInteractorSetup) TestProductInteractor_ShowAll(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Interactor Show All",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		products, err := s.PI.ShowAll()
		assert.Nil(t, err)

		assert.NotEmpty(t, products)
	})
}

func (s *ProductInteractorSetup) TestProductInteractor_ShowOne(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Interactor Show One",
		Body: model.Product{
			Name: "Test",
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		product, err := s.PI.ShowOne(s.Suite.Data.Body.Name)
		assert.Nil(t, err)

		assert.NotEmpty(t, product)
		assert.Equal(t, s.Suite.Data.Body.Name, product.Name)
	})
}

func (s *ProductInteractorSetup) TestProductInteractor_ShowOneBydId(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Interactor Show One By Id",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		product, err := s.PI.ShowOneBydId(s.UID)
		assert.Nil(t, err)

		assert.NotEmpty(t, product)
		assert.Equal(t, s.UID, product.Id)
	})
}

func (s *ProductInteractorSetup) TestProductInteractor_Modify(t *testing.T) {
	var product model.Product
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Interactor Modify",
		Body: model.Product{
			Id:          s.UID,
			Name:        "NewTest",
			Description: "NewTest",
			Price:       200,
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		retour := s.DB.Table("products").Where("id = ?", s.Suite.Data.Body.Id).First(&product)
		assert.Nil(t, retour.Error)

		err := s.PI.Modify(product, s.Suite.Data.Body)
		assert.Nil(t, err)

		result := s.DB.Table("products").Where("id = ?", s.Suite.Data.Body.Id).First(&s.NewProduct)
		assert.Nil(t, result.Error)

		assert.NotEmpty(t, s.NewProduct)

		assert.Equal(t, product.Id, s.NewProduct.Id)
		assert.Equal(t, product.Stock, s.NewProduct.Stock)
		assert.Equal(t, product.Categorie, s.NewProduct.Categorie)
		assert.Equal(t, product.Image, s.NewProduct.Image)

		assert.NotEqual(t, product.Name, s.NewProduct.Name)
		assert.NotEqual(t, product.Description, s.NewProduct.Description)
		assert.NotEqual(t, product.Price, s.NewProduct.Price)
	})
}

func (s *ProductInteractorSetup) TestProductInteractor_Destroy(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Interactor Destory",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		err := s.PI.Destroy(s.UID)
		assert.Nil(t, err)

		retour := s.DB.Table("products").Where("id = ?", s.UID).First(&s.NewProduct)
		assert.NotNil(t, retour.Error)
	})
}

func TestProductInteractor(t *testing.T) {
	// Initialisation de l'environnement de test
	s := NewProductInteractorSetup(t)

	// CRUD
	s.TestProductInteractor_Insert(t)
	s.TestProductInteractor_ShowAll(t)
	s.TestProductInteractor_ShowOne(t)
	s.TestProductInteractor_ShowOneBydId(t)
	s.TestProductInteractor_Modify(t)
	s.TestProductInteractor_Destroy(t)

	// Suppression du fichier .db
	defer func() {
		if err := os.Remove("product_interactor_test.db"); err != nil {
			t.Log(err.Error())
		}
	}()
}
