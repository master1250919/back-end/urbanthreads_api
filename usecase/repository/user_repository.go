package repository

import "gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"

type IUserRepository interface {
	IsExist(email string) bool

	GetAll() (model.Users, error)
	GetOneId(uid string) (model.User, error)
	GetOne(email string) (model.User, error)

	Create(data model.User) (model.User, error)
	Update(user model.User) error
	Delete(uid string) error

	Login(email, password string) error
}
