package repository

import "gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"

type IProductRepository interface {
	IsExist(name string, is string) bool

	GetAll() (model.Products, error)
	GetOne(name string) (model.Product, error)
	GetOneById(uid string) (model.Product, error)

	Create(data model.Product) error
	Update(data model.Product) error
	Delete(uid string) error

	InsertInRedis(products model.Products) error
	GetAllInRedis() (model.Products, error)
}
