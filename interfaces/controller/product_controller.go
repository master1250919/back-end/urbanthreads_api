package controller

import (
	"context"
	"github.com/redis/go-redis/v9"
	"gitlab.com/Titouan-Esc/api_common/controller"
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/logger"
	"gitlab.com/Titouan-Esc/api_common/utils"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/interfaces/repository"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/usecase/interactor"
	"net/http"
)

type ProductController struct {
	ProductInteractor interactor.ProductInteractor
	Logger            *logger.Log
}

func NewProductController(sql handler.SQLHandler, logger *logger.Log, redis *redis.Client, ctx context.Context) *ProductController {
	return &ProductController{
		ProductInteractor: interactor.ProductInteractor{
			ProductRepository: &repository.ProductRepository{
				Sql:     sql,
				Logger:  logger,
				Redis:   redis,
				Context: ctx,
			},
		},
		Logger: logger,
	}
}

// ShowAll
// @Summary						Show all products
// @Description					Retrieve all products in the database
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Products
// @Success						200 {object} model.Products{}
// @Failure						500 {object} model.ResponsBody{}
// @Router						/product/getAll [post]
func (pc *ProductController) ShowAll(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, pc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	products, err := pc.ProductInteractor.ShowAll()
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK, products)
}

// ShowOne
// @Summary						Show one product
// @Description					Retrieve one product in the database with his name
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Products
// @Success						200 {object} model.Product{}
// @Failure						500 {object} model.ResponsBody{}
// @Router						/product/getOne [post]
func (pc *ProductController) ShowOne(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, pc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	body := utils.ReadBody[model.Product](manager.Body)

	product, err := pc.ProductInteractor.ShowOne(body.Name)
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
	}

	manager.Respons().Build(http.StatusOK, product)
}

// Insert
// @Summary						Insert one product
// @Description					Create one product in the database
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Products
// @Success						201
// @Failure						500 {object} model.ResponsBody{}
// @Router						/product/create [post]
func (pc *ProductController) Insert(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, pc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	body := utils.ReadBody[model.Product](manager.Body)

	if err := pc.ProductInteractor.Insert(body); err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusCreated)
}

// Modify
// @Summary						Modify one product
// @Description					Update one product in the database
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Products
// @Success						200
// @Failure						500 {object} model.ResponsBody{}
// @Router						/product/update [post]
func (pc *ProductController) Modify(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, pc.Logger, false)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	body := utils.ReadBody[model.Product](manager.Body)

	product, err := pc.ProductInteractor.ShowOneBydId(body.Id)
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	if err := pc.ProductInteractor.Modify(product, body); err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK)
}

// Destroy
// @Summary						Destroy one product
// @Description					Delete one product in the database
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Products
// @Success						200
// @Failure						500 {object} model.ResponsBody{}
// @Router						/product/delete [post]
func (pc *ProductController) Destroy(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, pc.Logger, false)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	body := utils.ReadBody[model.Product](manager.Body)

	if err := pc.ProductInteractor.Destroy(body.Id); err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK)
}

// ShowAllAndInsertInDBRedis
// @Summary						Insert in Redis
// @Description					Retrieve products from DB and insert in Redis DB
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Products
// @Success						200
// @Failure						500 {object} model.ResponsBody{}
// @Router						/product/insertRedis [post]
func (pc *ProductController) ShowAllAndInsertInDBRedis(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, pc.Logger, false)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	products, err := pc.ProductInteractor.ShowAll()
	if err != nil {
		manager.Respons().Build(http.StatusBadRequest, err.Error())
		return
	}

	if err := pc.ProductInteractor.ShowAllAndInsertInDBRedis(products); err != nil {
		manager.Respons().Build(http.StatusBadRequest, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK)
}

// ShowAllInRedis
// @Summary						ShowAll in Redis
// @Description					Retrieve all products from Redis DB
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Products
// @Success						200 {object} model.Products{}
// @Failure						500 {object} model.ResponsBody{}
// @Router						/product/getAllRedis [post]
func (pc *ProductController) ShowAllInRedis(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, pc.Logger, false)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	products, err := pc.ProductInteractor.ShowAllInRedis()
	if err != nil {
		manager.Respons().Build(http.StatusBadRequest, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK, products)
}
