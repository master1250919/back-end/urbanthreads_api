package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"unicode/utf8"

	"gitlab.com/Titouan-Esc/api_common/sms"

	"gitlab.com/Titouan-Esc/api_common/controller"
	"gitlab.com/Titouan-Esc/api_common/email"
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/logger"
	"gitlab.com/Titouan-Esc/api_common/middlewares"
	"gitlab.com/Titouan-Esc/api_common/utils"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/interfaces/repository"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/usecase/interactor"
)

type UserController struct {
	UserInteractor interactor.UserInteractor
	Logger         *logger.Log
}

func NewUserController(sql handler.SQLHandler, logger *logger.Log) *UserController {
	return &UserController{
		UserInteractor: interactor.UserInteractor{
			UserRepository: &repository.UserRepository{
				Sql:    sql,
				Logger: logger,
			},
		},
		Logger: logger,
	}
}

// ShowAll
// @Summary						Show all users
// @Description					Retrieve all users in the database
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Users
// @Success						200 {object} model.Users{}
// @Failure						500 {object} model.ResponsBody{}
// @Router						/user/getAll [post]
func (uc *UserController) ShowAll(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	users, err := uc.UserInteractor.ShowAll()
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK, users)
}

// ShowOne
// @Summary						Show one user
// @Description					Retrieve one users in the database with his e-mail
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Users
// @Success						200 {object} model.User{}
// @Failure						500 {object} model.ResponsBody{}
// @Router						/user/getOne [post]
func (uc *UserController) ShowOne(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	user, err := uc.UserInteractor.ShowOneById(manager.UserId)
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	var newUser model.User
	newUser.Id = user.Id
	newUser.Email = user.Email
	newUser.Name = user.Name
	newUser.Phone = user.Phone

	manager.Respons().Build(http.StatusOK, newUser)
}

// Insert
// @Summary						Create one user
// @Description					Create user in the database
// @Produce						application/json
// @Tags						Users
// @Success						201 {object} model.User{}
// @Failure						500 {object} model.ResponsBody{}
// @Router						/user/create [post]
func (uc *UserController) Insert(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger, false)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	body := utils.ReadBody[model.User](manager.Body)

	user, err := uc.UserInteractor.Insert(body)
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusCreated, user)
}

// Modify
// @Summary						Update one user
// @Description					Update user in the database
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Users
// @Success						200 {object} model.User{}
// @Failure						500 {object} model.ResponsBody{}
// @Router						/user/update [post]
func (uc *UserController) Modify(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	user, err := uc.UserInteractor.ShowOneById(manager.UserId)
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	body := utils.ReadBody[model.User](manager.Body)

	if err := uc.UserInteractor.Modify(user, body); err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK)
}

// Destroy
// @Summary						Delete one user
// @Description					Delete user in the database
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Users
// @Success						200 {object} model.User{}
// @Failure						500 {object} model.ResponsBody{}
// @Router						/user/delete [post]
func (uc *UserController) Destroy(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	if err := uc.UserInteractor.Destroy(manager.UserId); err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK)
}

// Connect
// @Summary						login one user
// @Description					Login user to the application
// @Produce						application/json
// @Tags						Users
// @Success						200 {object} model.User{}
// @Failure						500 {object} model.ResponsBody{}
// @Router						/user/login [post]
func (uc *UserController) Connect(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger, false)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	body := utils.ReadBody[model.User](manager.Body)

	if err := uc.UserInteractor.Connect(body.Email, body.Password); err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	token, err := middlewares.CreateJwt(body.Email)
	if err != nil {
		uc.Logger.Error(err.Error())
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK, token)
}

// PasswordReset
// @Summary						reset password of user
// @Description					when one user forgotten his password, i send e-mail for changing his password
// @Produce						application/json
// @Tags						Users
// @Success						200 {object} model.User{}
// @Failure						500 {object} model.ResponsBody{}
// @Router						/user/passwordReset [post]
func (uc *UserController) PasswordReset(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger, false)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	body := utils.ReadBody[model.User](manager.Body)

	r := email.NewRequest("titouanescorneboueu@gmail.com", body.Email, "Reset Password", "", "./templates/template.html")

	if err := r.SendMail(); err != nil {
		//uc.Logger.Error(err.Error())
		manager.Respons().Build(http.StatusInternalServerError, err.Error()).Logging()
		return
	}

	manager.Respons().Build(http.StatusOK, "ok")
}

// SendSMS
// @Summary						sending sms to user
// @Description					when one user forgotten his password, i send e-mail for changing his password
// @Param						Authorization header string true "Token generate to connection"
// @Produce						application/json
// @Tags						Users
// @Success						200 {object} model.User{}
// @Failure						500 {object} model.ResponsBody{}
// @Router						/user/passwordReset [post]
func (uc *UserController) SendSMS(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	body := utils.ReadBody[model.User](manager.Body)

	// Enlever le premier char du numéro de téléphone
	_, i := utf8.DecodeRuneInString(body.Phone)
	s := body.Phone[i:]

	// Ajouter +33 car nous sommes en France
	newPhone := fmt.Sprintf("+33%s", s)

	response, err := sms.SendSMS(newPhone, "Ca va enculé de merde va")
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error()).Logging()
		return
	}

	manager.Respons().Build(http.StatusOK, response)
}

// PING
// @Summary						Send Pong
// @Description					Verify if server is running with a ping
// @Produce						application/json
// @Tags						PING
// @Success						200 {string} string "PONG"
// @Failure						500
// @Router						/ping [post]
func (uc *UserController) PING(res http.ResponseWriter, _ *http.Request) {

	fmt.Println("Ping exécuté")

	type PingResponse struct {
		Message string `json:"message"`
	}

	pingResponse := PingResponse{
		Message: "PONG",
	}

	responseJSON, err := json.Marshal(pingResponse)
	if err != nil {
		http.Error(res, "Erreur de merde", http.StatusInternalServerError)
		return
	}

	res.Header().Set("Content-Type", "application/json")

	res.Write(responseJSON)
}
