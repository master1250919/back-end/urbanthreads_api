package repository

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/logger"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"
	"gorm.io/gorm"
)

type ProductRepository struct {
	Sql     handler.SQLHandler
	Logger  *logger.Log
	Redis   *redis.Client
	Context context.Context
}

func NewProductRepository(database *gorm.DB, rdb *redis.Client) *ProductRepository {
	return &ProductRepository{
		Sql: handler.SQLHandler{
			Db: database,
		},
		Redis:   rdb,
		Context: context.Background(),
	}
}

func (p ProductRepository) GetOneById(uid string) (model.Product, error) {
	var product model.Product

	if retour := p.Sql.Db.Table("products").Where("id = ?", uid).First(&product); retour.Error != nil {
		p.Logger.Error(retour.Error)
		return product, retour.Error
	}

	return product, nil
}

func (p ProductRepository) GetAllInRedis() (model.Products, error) {
	var products model.Products

	keys, err := p.Redis.Keys(p.Context, "product_*").Result()
	if err != nil {
		return products, err
	}

	for _, key := range keys {
		var product model.Product

		val, err := p.Redis.Get(p.Context, key).Result()
		if err != nil {
			p.Logger.Error(err.Error())
			return products, err
		}

		if err := product.UnmarshalBinary([]byte(val)); err != nil {
			p.Logger.Error(err.Error())
			return products, err
		}

		products = append(products, product)
	}

	return products, nil
}

func (p ProductRepository) InsertInRedis(products model.Products) error {
	count := 0

	if err := p.Redis.FlushAll(p.Context).Err(); err != nil {
		p.Logger.Error(err)
	}

	for _, value := range products {

		data, err := value.MarshalBinary()
		if err != nil {
			return err
		}

		key := fmt.Sprintf("product_%d", count)

		if err := p.Redis.Set(p.Context, key, data, 0).Err(); err != nil {
			p.Logger.Error(err)
			return err
		}

		count++
	}

	return nil
}

func (p ProductRepository) IsExist(name string, is string) bool {
	var product model.Product

	retour := p.Sql.Db.Table("products").Where(fmt.Sprintf(`%s = '%s'`, is, name)).First(&product)
	if retour.Error != nil {
		return false
	}

	if product.Id == "" {
		p.Logger.Error("Product already exist in th database")
		return false
	} else {
		return true
	}
}

func (p ProductRepository) GetAll() (model.Products, error) {
	var products model.Products

	if retour := p.Sql.Db.Table("products").Find(&products); retour.Error != nil {
		p.Logger.Error(retour.Error)
		return model.Products{}, retour.Error
	}

	return products, nil
}

func (p ProductRepository) GetOne(name string) (model.Product, error) {
	var product model.Product

	if retour := p.Sql.Db.Table("products").Where("name = ?", name).First(&product); retour.Error != nil {
		return model.Product{}, retour.Error
	}

	return product, nil
}

func (p ProductRepository) Create(data model.Product) error {

	data.Id = uuid.New().String()

	if retour := p.Sql.Db.Table("products").Create(&data); retour.Error != nil {
		p.Logger.Error(retour.Error)
		return retour.Error
	}

	return nil
}

func (p ProductRepository) Update(product model.Product) error {
	if retour := p.Sql.Db.Table("products").Save(&product); retour.Error != nil {
		p.Logger.Error(retour.Error)
		return retour.Error
	}

	return nil
}

func (p ProductRepository) Delete(uid string) error {
	var product model.Product

	if retour := p.Sql.Db.Table("products").Where("id = ?", uid).Delete(&product); retour.Error != nil {
		p.Logger.Error(retour.Error)
		return retour.Error
	}

	return nil
}
