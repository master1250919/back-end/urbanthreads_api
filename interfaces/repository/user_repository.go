package repository

import (
	"github.com/google/uuid"
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/logger"
	"gitlab.com/Titouan-Esc/api_common/middlewares"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"
	"gorm.io/gorm"
)

type UserRepository struct {
	Sql    handler.SQLHandler
	Logger *logger.Log
}

func NewUserRepository(db *gorm.DB) *UserRepository {
	return &UserRepository{
		Sql: handler.SQLHandler{
			Db: db,
		},
	}
}

func (u UserRepository) GetOneId(uid string) (model.User, error) {
	var user model.User

	if retour := u.Sql.Db.Table("users").Where("id = ?", uid).First(&user); retour.Error != nil {
		u.Logger.Warning(retour.Error)
		return user, retour.Error
	}

	return user, nil
}

func (u UserRepository) IsExist(email string) bool {
	var user model.User

	retour := u.Sql.Db.Table("users").Where("email = ?", email).First(&user)
	if retour.Error != nil {
		return false
	}

	if user.Id == "" {
		u.Logger.Error("User not exist in database")
		return false
	} else {
		return true
	}
}

func (u UserRepository) Create(data model.User) (model.User, error) {
	var user model.User

	data.Id = uuid.New().String()
	data.Password = middlewares.HashPassword(data.Password)

	if retour := u.Sql.Db.Table("users").Create(&data); retour.Error != nil {
		u.Logger.Error(retour.Error)
		return user, retour.Error
	}

	user.Id = data.Id
	user.Name = data.Name
	user.Email = data.Email
	user.Phone = data.Phone

	return user, nil
}

func (u UserRepository) Update(user model.User) error {
	if retour := u.Sql.Db.Table("users").Save(&user); retour.Error != nil {
		u.Logger.Error(retour.Error)
		return retour.Error
	}

	return nil
}

func (u UserRepository) Delete(uid string) error {
	var user model.User

	if retour := u.Sql.Db.Table("users").Where("id = ?", uid).Delete(&user); retour.Error != nil {
		u.Logger.Error(retour.Error)
		return retour.Error
	}

	return nil
}

func (u UserRepository) Login(email, password string) error {
	var user model.User

	if retour := u.Sql.Db.Table("users").Where("email = ?", email).First(&user); retour.Error != nil {
		u.Logger.Error(retour.Error)
		return retour.Error
	}

	if err := middlewares.ValidatePassword(password, user.Password); err != nil {
		u.Logger.Error(err.Error())
		return err
	}

	return nil
}

func (u UserRepository) GetAll() (model.Users, error) {
	var users model.Users

	if retour := u.Sql.Db.Table("users").Find(&users); retour.Error != nil {
		u.Logger.Error(retour.Error)
		return model.Users{}, retour.Error
	}

	return users, nil
}

func (u UserRepository) GetOne(email string) (model.User, error) {
	var user model.User

	if retour := u.Sql.Db.Table("users").Where("email = ?", email).Select("id, name, email, phone").First(&user); retour.Error != nil {
		u.Logger.Error(retour.Error)
		return user, retour.Error
	}

	return user, nil
}
