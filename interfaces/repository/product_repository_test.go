package repository

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/Titouan-Esc/api_common/test"
	"gitlab.com/Titouan-Esc/api_common/utils"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"
	"gorm.io/gorm"
	"os"
	"testing"
)

type ProductRepositorySetup struct {
	PR         *ProductRepository
	DB         *gorm.DB
	Suite      *test.Suite[model.Product]
	NewProduct model.Product
	UID        string
}

func NewProductRepositorySetup(t *testing.T) *ProductRepositorySetup {
	suite, db, redis, err := test.SetUp[model.Product]("product_repository.db", true)
	assert.Nil(t, err)

	pr := NewProductRepository(db, redis)

	errMigrate := db.AutoMigrate(model.Product{})
	assert.Nil(t, errMigrate)

	return &ProductRepositorySetup{
		PR:    pr,
		DB:    db,
		Suite: suite,
	}
}

func (s *ProductRepositorySetup) TestProductRepository_Create(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Repository Create",
		Body: model.Product{
			Name:        "Test",
			Description: "Test",
			Price:       100,
			Stock:       100,
			Categorie:   "test",
			Image:       []byte("test"),
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		err := s.PR.Create(s.Suite.Data.Body)
		assert.Nil(t, err)

		retour := s.DB.Table("products").Where("name = ?", s.Suite.Data.Body.Name).First(&s.NewProduct)
		assert.Nil(t, retour.Error)

		s.UID = s.NewProduct.Id

		assert.Equal(t, s.Suite.Data.Body.Name, s.NewProduct.Name)
		assert.Equal(t, s.Suite.Data.Body.Description, s.NewProduct.Description)
		assert.Equal(t, s.Suite.Data.Body.Price, s.NewProduct.Price)
		assert.Equal(t, s.Suite.Data.Body.Stock, s.NewProduct.Stock)
		assert.Equal(t, s.Suite.Data.Body.Categorie, s.NewProduct.Categorie)
		assert.Equal(t, s.Suite.Data.Body.Image, s.NewProduct.Image)
	})
}

func (s *ProductRepositorySetup) TestProductRepository_InsertInRedis(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Repository Insert In Redis",
	}

	products := model.Products{
		{
			Id:          "test1",
			Name:        "test1",
			Description: "test1",
			Price:       100,
			Stock:       100,
			Categorie:   "test1",
			Image:       []byte("test"),
		},
		{
			Id:          "test2",
			Name:        "test2",
			Description: "test2",
			Price:       100,
			Stock:       100,
			Categorie:   "test2",
			Image:       []byte("test"),
		},
		{
			Id:          "test3",
			Name:        "test3",
			Description: "test3",
			Price:       100,
			Stock:       100,
			Categorie:   "test3",
			Image:       []byte("test"),
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		err := s.PR.InsertInRedis(products)
		assert.Nil(t, err)

		products, err := s.PR.GetAllInRedis()
		assert.Nil(t, err)

		assert.NotEmpty(t, products)
	})
}

func (s *ProductRepositorySetup) TestProductRepository_GetAll(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Repository Get All",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		products, err := s.PR.GetAll()
		assert.Nil(t, err)

		assert.NotEmpty(t, products)
	})
}

func (s *ProductRepositorySetup) TestProductRepository_GetOne(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Repository Get One",
		Body: model.Product{
			Name: "Test",
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		product, err := s.PR.GetOne(s.Suite.Data.Body.Name)
		assert.Nil(t, err)

		assert.Equal(t, s.Suite.Data.Body.Name, product.Name)
		assert.Equal(t, s.UID, product.Id)
	})
}

func (s *ProductRepositorySetup) TestProductRepository_GetOneById(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Repository Get One By Id",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		product, err := s.PR.GetOneById(s.UID)
		assert.Nil(t, err)

		assert.Equal(t, s.UID, product.Id)
	})
}

func (s *ProductRepositorySetup) TestProductRepository_GetAllInRedis(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Repository Get All In Redis",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		products, err := s.PR.GetAllInRedis()
		assert.Nil(t, err)

		assert.NotEmpty(t, products)
	})
}

func (s *ProductRepositorySetup) TestProductRepository_Update(t *testing.T) {
	var product model.Product
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Repository Update",
		Body: model.Product{
			Name:        "NewTest",
			Description: "New Test",
			Price:       200,
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		retour := s.DB.Table("products").Where("id = ?", s.UID).First(&product)
		assert.Nil(t, retour.Error)

		errBind := utils.Bind(&product, s.Suite.Data.Body)
		assert.Nil(t, errBind)

		err := s.PR.Update(product)
		assert.Nil(t, err)

		result := s.DB.Table("products").Where("id = ?", s.UID).First(&s.NewProduct)
		assert.Nil(t, result.Error)

		assert.Equal(t, s.Suite.Data.Body.Name, s.NewProduct.Name)
		assert.Equal(t, s.Suite.Data.Body.Description, s.NewProduct.Description)
		assert.Equal(t, s.Suite.Data.Body.Price, s.NewProduct.Price)
	})
}

func (s *ProductRepositorySetup) TestProductRepository_Delete(t *testing.T) {
	s.Suite.Data = test.Data[model.Product]{
		Name: "Product Repository Delete",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		err := s.PR.Delete(s.UID)
		assert.Nil(t, err)

		retour := s.DB.Table("products").Where("id = ?", s.UID).First(&s.NewProduct)
		assert.Error(t, retour.Error)
	})
}

func TestProductRepository(t *testing.T) {
	s := NewProductRepositorySetup(t)

	// TEST
	s.TestProductRepository_Create(t)
	s.TestProductRepository_InsertInRedis(t)
	s.TestProductRepository_GetAll(t)
	s.TestProductRepository_GetOne(t)
	s.TestProductRepository_GetOneById(t)
	s.TestProductRepository_GetAllInRedis(t)
	s.TestProductRepository_Update(t)
	s.TestProductRepository_Delete(t)

	defer func() {
		if err := os.Remove("product_repository.db"); err != nil {
			t.Log(err.Error())
		}
	}()
}
