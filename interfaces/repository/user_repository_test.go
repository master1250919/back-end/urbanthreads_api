package repository

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/Titouan-Esc/api_common/test"
	"gitlab.com/Titouan-Esc/api_common/utils"
	"gitlab.com/Titouan-Esc/master1250919/back-end/urbanthreads_api/domain/model"
	"gorm.io/gorm"
	"os"
	"testing"
)

type UserRepositorySetup struct {
	UR      *UserRepository
	DB      *gorm.DB
	Suite   *test.Suite[model.User]
	NewUser model.User
	UID     string
}

func NewUserRepositorySetup(t *testing.T) *UserRepositorySetup {
	suite, db, redis, err := test.SetUp[model.User]("user_repository_test.db")
	assert.Nil(t, err)
	assert.Nil(t, redis)

	ur := NewUserRepository(db)

	errMigrate := db.AutoMigrate(model.User{})
	assert.Nil(t, errMigrate)

	return &UserRepositorySetup{
		UR:    ur,
		DB:    db,
		Suite: suite,
	}
}

func (s *UserRepositorySetup) TestUserRepository_Create(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Repository Create",
		Body: model.User{
			Name:     "Test",
			Email:    "test@test.com",
			Password: "test",
			Phone:    "00.00.00.00.00",
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		user, err := s.UR.Create(s.Suite.Data.Body)
		assert.Nil(t, err)

		s.UID = user.Id

		retour := s.DB.Table("users").Where("id = ?", user.Id).First(&s.NewUser)
		assert.Nil(t, retour.Error)

		assert.Equal(t, user.Id, s.NewUser.Id)
		assert.Equal(t, user.Name, s.NewUser.Name)
		assert.Equal(t, user.Email, s.NewUser.Email)
		assert.Equal(t, user.Phone, s.NewUser.Phone)
	})
}

func (s *UserRepositorySetup) TestUserRepository_GetAll(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Repository Get All",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		users, err := s.UR.GetAll()
		assert.Nil(t, err)

		assert.NotEmpty(t, users)
	})
}

func (s *UserRepositorySetup) TestUserRepository_GetOne(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Repository Get One",
		Body: model.User{
			Email: "test@test.com",
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		user, err := s.UR.GetOne(s.Suite.Data.Body.Email)
		assert.Nil(t, err)

		assert.Equal(t, user.Email, s.Suite.Data.Body.Email)
		assert.Equal(t, user.Id, s.UID)
	})
}

func (s *UserRepositorySetup) TestUserRepository_GetOneId(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Repository Get One By Id",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		user, err := s.UR.GetOneId(s.UID)
		assert.Nil(t, err)

		assert.Equal(t, user.Id, s.UID)
	})
}

func (s *UserRepositorySetup) TestUserRepository_Login(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Repository Login",
		Body: model.User{
			Email:    "test@test.com",
			Password: "test",
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		err := s.UR.Login(s.Suite.Data.Body.Email, s.Suite.Data.Body.Password)
		assert.Nil(t, err)
	})
}

func (s *UserRepositorySetup) TestUserRepository_Update(t *testing.T) {
	var user model.User
	s.Suite.Data = test.Data[model.User]{
		Name: "User Repository Update",
		Body: model.User{
			Name:  "NewTest",
			Email: "newTest@test.com",
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		retour := s.DB.Table("users").Where("id = ?", s.UID).First(&user)
		assert.Nil(t, retour.Error)

		errBind := utils.Bind(&user, s.Suite.Data.Body)
		assert.Nil(t, errBind)

		err := s.UR.Update(user)
		assert.Nil(t, err)

		result := s.DB.Table("users").Where("id = ?", s.UID).First(&s.NewUser)
		assert.Nil(t, result.Error)

		assert.Equal(t, s.Suite.Data.Body.Name, s.NewUser.Name)
		assert.Equal(t, s.Suite.Data.Body.Email, s.NewUser.Email)
		assert.Equal(t, user.Password, s.NewUser.Password)
		assert.Equal(t, user.Phone, s.NewUser.Phone)
	})
}

func (s *UserRepositorySetup) TestUserRepository_Delete(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Repository Delete",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		err := s.UR.Delete(s.UID)
		assert.Nil(t, err)

		retour := s.DB.Table("users").Where("id = ?", s.UID).First(&s.NewUser)
		assert.NotNil(t, retour.Error)
	})
}

func TestUserRepository(t *testing.T) {
	s := NewUserRepositorySetup(t)

	// TEST
	s.TestUserRepository_Create(t)
	s.TestUserRepository_GetAll(t)
	s.TestUserRepository_GetOne(t)
	s.TestUserRepository_GetOneId(t)
	s.TestUserRepository_Login(t)
	s.TestUserRepository_Update(t)
	s.TestUserRepository_Delete(t)

	// Suppression du fichier .db
	defer func() {
		if err := os.Remove("user_repository_test.db"); err != nil {
			t.Log(err.Error())
		}
	}()
}
